import random
import argparse
import networkx as nx
import matplotlib.pyplot as plt
from pprint import pprint

class Graph(object):
    def __init__(self, nodes, edges=None, loops=False, multigraph=False, diagraph=False):
        self.nodes = nodes
        if edges:
            self.edges = edges
            self.edge_set = self._compute_edge_set()
        else:
            self.edges = []
            self.edge_set = set()
        self.loops = loops
        self.multigraph = multigraph
        self.diagraph = diagraph

    def _compute_edge_set(self):
        raise NotImplementedError()

    def add_edge(self, edge):
        if self.multigraph or edge not in self.edge_set:
            self.edges.append(edge)
            self.edge_set.add(edge)
            if not self.diagraph:
                self.edge_set.add(edge[::-1]) # add other direction to edge
            return True
        return False
    
    def make_random_edge(self):
        """Generate a random edge"""
        if self.loops:
            # with replacement
            random_edge = (random.choice(self.nodes), random.choice(self.nodes))
        else:
            # without replacement
            random_edge = tuple(random.sample(self.nodes, 2))
        return random_edge

    def add_random_edges(self, total_edges):
        """Add random edges, until total_edges reached"""
        while (len(self.edges) < total_edges):
            self.add_edge(self.make_random_edge())

    def sort_edges(self):
        """If undirected, sort order the nodes are listed in the edges."""
        if not self.diagraph:
            self.edges = [((t,s) if t < s else (s, t)) for s, t in self.edges]
        # sort by ascending (default)
        self.edges.sort()

    def random_walk(self, nodes, num_edges, loops=False, multigraph=False, diagraph=False):
        """Create a uniform spanning tree using random walk."""
        S, T = set(nodes), set()
        # pick a random node
        current_node = random.sample(S, 1).pop()
        S.remove(current_node)
        T.add(current_node)
        # call Graph
        graph = Graph(nodes, loops=loops, multigraph=multigraph, diagraph=diagraph)
        # Create a random connected graph
        while S:
            # random pick next node from the neighbors of the current node
            neighbor_node = random.sample(nodes, 1).pop()
            # if new node has not been visited, add the edge from current to new
            if neighbor_node not in T:
                edge = (current_node, neighbor_node)
                graph.add_edge(edge)
                S.remove(neighbor_node)
                T.add(neighbor_node)
            # Set the new node as the current node
            current_node = neighbor_node
        graph.add_random_edges(num_edges)
        return graph

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('nodes',
                        help='filename containing node labels (one per line) '
                             'OR integer number of nodes to generate')
    parser.add_argument('-e', '--edges', type=int,
                        help='number of edges (default is minimum possible)')
    parser.add_argument('-l', '--loops', action='store_true',
                        help='allow self-loop edges')
    parser.add_argument('-m', '--multigraph', action='store_true',
                        help='allow parallel edges between nodes')
    parser.add_argument('-d', '--digraph', action='store_true',
                        help='make edges unidirectional')
    parser.add_argument('--no-output', action='store_true',
                        help='do not display any output')
    parser.add_argument('-p', '--pretty', action='store_true',
                        help='print large graphs with each edge on a new line')
    args = parser.parse_args()

    # Nodes
    try:
        nodes = [x for x in range(int(args.nodes))]
    except ValueError:
        raise TypeError('nodes argument must be a filename or an integer')

    # Edges
    if args.edges is None:
        num_edges = len(nodes) - 1
    else:
        num_edges = args.edges
    
    # Run
    graph = Graph(nodes, None, args.loops, args.multigraph, args.digraph)
    g2 = graph.random_walk(nodes, num_edges, args.loops, args.multigraph, args.digraph)
    g = nx.Graph()
    g.add_nodes_from(g2.nodes)
    g.add_edges_from(g2.edges)

    # Display
    if not args.no_output:
        g2.sort_edges()
        if args.pretty:
            pprint(g2.nodes)
            pprint(g2.edges)
        else:
            print(g2.nodes)
            print(g2.edges)
            # print(g2.neighbors(1))

    pos = nx.spring_layout(g)
    nx.draw_networkx(g, pos)
    # nx.draw(g2) 
    plt.savefig("road-graph3.png")