import random
import math
from sys import exit


def altitude(x, y):
    return (((x**2) + (y**2)) / 80) + 5


def distance(i, j):
    return math.sqrt((nodes[i][0] - nodes[j][0]) ** 2 + (nodes[i][1] - nodes[j][1]) ** 2)


def creategraph():
    visited = [0]
    while len(visited) < len(nodes):
        length = 99999999
        idx1 = -1
        idx2 = -1
        for i in visited:
            for j in range(len(nodes)):
                if j in visited:
                    continue
                if distance(i, j) < length:
                    length = distance(i, j)
                    idx1 = i
                    idx2 = j
        edges.append((min(idx1, idx2), max(idx1, idx2)))
        if idx1 not in visited:
            visited.append(idx1)
        else:
            visited.append(idx2)


def on_segment(p, q, r):
    if r[0] <= max(p[0], q[0]) and r[0] >= min(p[0], q[0]) and r[1] <= max(p[1], q[1]) and r[1] >= min(p[1], q[1]):
        return True
    return False


def orientation(p, q, r):
    val = ((q[1] - p[1]) * (r[0] - q[0])) - ((q[0] - p[0]) * (r[1] - q[1]))
    if val == 0:
        return 0
    return 1 if val > 0 else -1


def intersects(X1, Y1, X2, Y2, X3, Y3, X4, Y4):
    seg1 = ((X1, Y1), (X2, Y2))
    seg2 = ((X3, Y3), (X4, Y4))
    p1, q1 = seg1
    p2, q2 = seg2
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)
    if o1 == 0 or o2 == 0 or o3 == 0 or o4 == 0:
        print("Found a zero\n")
    if o1 != o2 and o3 != o4:
        print("Found a match\n")
        return True
    if o1 == 0 and on_segment(p1, q1, p2):
        return True
    if o2 == 0 and on_segment(p1, q1, q2):
        return True
    if o3 == 0 and on_segment(p2, q2, p1):
        return True
    if o4 == 0 and on_segment(p2, q2, q1):
        return True
    return False



n = int(input("Enter the number of nodes:"))
m = int(input("Enter the number of edges:"))
nodes = []
edges = []
for i in range(n):
    while True:
        x = random.uniform(1, 20)
        y = random.uniform(1, 20)
        z = altitude(x, y)
        if (x, y, z) not in nodes:
            nodes.append((x, y, z))
            break
creategraph()
# try drawing graph here
m -= (n - 1)
if m < 0:
    print("Not enough edges.")
    exit(0)
for x in range(len(nodes)):
    if m < 0:
        break
    for y in range(len(nodes)):
        if x == y:
            continue
        if m < 0:
            break
        B = True
        for i in edges:
            if i[0] == x or i[0] == y or i[1] == x or i[1] == y:
                continue
            if intersects(nodes[x][0], nodes[x][1], nodes[y][0], nodes[y][1], nodes[i[0]][0], nodes[i[0]][1], nodes[i[1]][0], nodes[i[1]][1]):
                B = False
                break
        if B:
            if m < 0:
                break
            m -= 1
            if m < 0:
                break
            edges.append((min(x, y), max(x, y)))
            break
for i in range(len(nodes)):
    m -= 1
    if m < 0:
        break
    dis = 99999999
    idx = -1
    for j in range(len(nodes)):
        if distance(i, j) <= dis:
            dis = distance(i, j)
            idx = j
    edges.append((min(idx, i), max(i, idx)))
#for i in nodes:
    #print("{0}, {1}, {2}".format(i[0], i[1], i[2]))
#for i in edges:
    #print("{0}, {1}".format(i[0], i[1]))

import matplotlib.pyplot as plt

import networkx as nx
g = nx.Graph()
g.add_nodes_from(nodes[0])
g.add_edges_from(edges)
pos = nx.spring_layout(g)
nx.draw_networkx(g, pos)
plt.savefig("sol6.png")
from pprint import pprint
pprint(g.nodes)

x = [i[0] for i in nodes]
y = [i[1] for i in nodes]
plt.plot(x, y, 'ro')
plt.axis('equal')
x_line = []
for i in edges:
    x_line.append(nodes[i[0]][0])
    x_line.append(nodes[i[1]][0])
y_line = []
for i in edges:
    y_line.append(nodes[i[0]][1])
    y_line.append(nodes[i[1]][1])
for i in range(0, len(x_line), 2):
    plt.plot(x_line[i:i+2], y_line[i:i+2], 'ro-')
# plt.show()
# plt.savefig("sol2.png")
