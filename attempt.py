import networkx
import random
import math
from sys import exit

class Attempt():
    def __init__(self, nodes, edges=None):
        self.nodes = nodes
        if edges:
            self.edges = edges
        else:
            self.edges = nodes + 1
        if self.edges < nodes - 1:
            print("Not enough edges!")
            exit(0)
        self.nodes_array = []
        self.edges_array = []
        self._create_nodes()
        self._create_graph()
        #self._create_graph2()
        self.remaining_edges = self.edges - (self.nodes - 1)

    # Create the altitude from the x,y positions
    def _create_altitude(self, x, y):
        # ** is exponent x*x, for relative altitude
        return (((x**2) + (y**2)) / 80) + 5

    # Create a random x,y positions, and create nodes_array
    def _create_nodes(self):
        nodesSet = set([x for x in range(int(nodes))])
        while nodesSet:
            nodesSet.pop()
            x = round(random.uniform(1, 20), 1)
            y = round(random.uniform(1, 20), 1)
            z = self._create_altitude(x, y)
            if (x, y, z) not in self.nodes_array:
                self.nodes_array.append((x, y, z))
        return self.nodes_array
    
    # Create a distance method: get the euclidean distance between 2 points
    def _distance(self, o, p):
        s1 = (self.nodes_array[o][0] - self.nodes_array[p][0]) ** 2
        s2 = (self.nodes_array[o][1] - self.nodes_array[p][1]) ** 2
        return math.sqrt(s1 + s2)

    def _create_graph2(self):
        nodesArray = [x for x in range(int(self.nodes))]
        N, V = set(nodesArray), set()
        # pick a random node
        current_node = random.sample(N, 1).pop()
        N.remove(current_node)
        V.add(current_node)
        while N:
            distance = 999
            #indx1 = -1
            #indx2 = -1
            # random pick next node from the neighbors of the current node
            neighbor_node = random.sample(N, 1).pop()
            # if new node has not been visited, add the edge from current to new
            if neighbor_node not in V:
                current_distance = self._distance(current_node, neighbor_node)
                if current_distance < distance:
                    distance = current_distance
                    indx1 = current_node
                    indx2 = neighbor_node
                # find the shortest distance
                for i in N:
                    current_distance = self._distance(current_node, i)
                    if current_distance < distance:
                        distance = current_distance
                        indx1 = current_node
                        indx2 = i
            self.edges_array.append((min(indx1, indx2), max(indx1, indx2)))
            N.remove(neighbor_node)
            #V.add(neighbor_node)
            current_node = neighbor_node
            if indx1 not in V:
                V.add(indx1)
            else:
                V.add(indx2)

    # Create a graph
    def _create_graph(self):
        visited = [0]
        node_length = len(self.nodes_array)
        while len(visited) < node_length:
            distance = 999
            indx1 = -1
            indx2 = -1
            for i in visited:
                for j in range(node_length):
                    if j in visited:
                        continue
                    if self._distance(i, j) < distance:
                        distance = self._distance(i, j)
                        indx1 = i
                        indx2 = j
            self.edges_array.append((min(indx1, indx2), max(indx1, indx2)))
            if indx1 not in visited:
                visited.append(indx1)
            else:
                visited.append(indx2)

    def _orientation(self, p, q, r):
        val = ((q[1] - p[1]) * (r[0] - q[0])) - ((q[0] - p[0]) * (r[1] - q[1]))
        if val == 0:
            return 0
        return 1 if val > 0 else -1

    def _intersects(self, X1, Y1, X2, Y2, X3, Y3, X4, Y4):
        seg1 = ((X1, Y1), (X2, Y2))
        seg2 = ((X3, Y3), (X4, Y4))
        p1, q1 = seg1
        p2, q2 = seg2
        o1 = self._orientation(p1, q1, p2)
        o2 = self._orientation(p1, q1, q2)
        o3 = self._orientation(p2, q2, p1)
        o4 = self._orientation(p2, q2, q1)
        if o1 != o2 and o3 != o4:
            return True
        return False

    # pick 2 points randomly and create an edges between them, 
    # if this new created edge won't intersect with the already constructed edges
    def add_edges(self):
        for x in range(len(self.nodes_array)):
            if self.remaining_edges < 0:
                break
            for y in range(len(self.nodes_array)):
                if x == y:
                    continue
                if self.remaining_edges < 0:
                    break
                B = True
                for i in self.edges_array:
                    if i[0] == x or i[0] == y or i[1] == x or i[1] == y:
                        continue
                    # Call intersects funtion
                    if self._intersects(self.nodes_array[x][0], self.nodes_array[x][1],
                    self.nodes_array[y][0], self.nodes_array[y][1],
                    self.nodes_array[i[0]][0], self.nodes_array[i[0]][1],
                    self.nodes_array[i[1]][0], self.nodes_array[i[1]][1]):
                        B = False
                        break
                if B:
                    if self.remaining_edges < 0:
                        break
                    if (min(x, y), max(x, y)) in self.edges_array:
                        continue
                    self.remaining_edges -= 1
                    if self.remaining_edges < 0:
                        break
                    self.edges_array.append((min(x, y), max(x, y)))

    def add_remaining_edges(self):
        for j in range(500):
            if self.remaining_edges < 0:
                break
            B = True
            x = random.randint(0, self.nodes - 1)
            y = random.randint(0, self.nodes - 1)
            if x == y:
                continue
            for i in self.edges_array:
                if i[0] == x or i[0] == y or i[1] == x or i[1] == y:
                    continue
                # Call intersects funtion
                if self._intersects(self.nodes_array[x][0], self.nodes_array[x][1],
                self.nodes_array[y][0], self.nodes_array[y][1],
                self.nodes_array[i[0]][0], self.nodes_array[i[0]][1],
                self.nodes_array[i[1]][0], self.nodes_array[i[1]][1]):
                    B = False
                    break
                if B:
                    if self.remaining_edges < 0:
                        break
                    if (min(x, y), max(x, y)) in self.edges_array:
                        continue
                    self.remaining_edges -= 1
                    if self.remaining_edges < 0:
                        break
                    self.edges_array.append((min(x, y), max(x, y)))

    def get_nodes(self):
        return self.nodes_array

    def get_edges(self):
        return self.edges_array

    def draw_map(self):
        import matplotlib.pyplot as plt
        import networkx as nx
        g = nx.Graph() #empty graph
        for i in range(len(self.nodes_array)):
            g.add_node(i, key=self.nodes_array[i])
        for i in self.edges_array:
            g.add_edge(i[0], i[1])
        pos = nx.spring_layout(g)
        nx.draw_networkx(g, pos)
        plt.savefig("attempt-9.png")


at = Attempt(30, 70)
# nodes = at._create_nodes()
#distance = at._distance(0,2)
#print(distance)
at.add_edges()
at.add_remaining_edges()
nodes = at.get_nodes()
for i in nodes:
    print("{0}, {1}, {2}".format(i[0], i[1], i[2]))

edges = at.get_edges()
#print(edges)
for i in edges:
    # print("{0}, {1}".format(i[0], i[1]))
    print("{0}, {1}".format(i[0], i[1]))

#at.add_edges()
#at.add_remaining_edges()
at.draw_map()