from __future__ import division
from random import random
import matplotlib.pyplot as plt

def line(p1, p2):
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return A, B, -C

def intersection(L1, L2):
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x,y
    else:
        return False


def drawGraph(xyzList,numberOfNodes):
    x = []
    y = []
    nodesList = []
    for data in xyzList:
        x.append(data[0])
        y.append(data[1])
        nodesList.append([data[0],data[1]])
    plt.scatter(x, y, color='r')

    intersectingPointX = []
    intersectingPointY = []

    for i in range(len(nodesList)):

        if i != len(nodesList)-1:
            pointA = nodesList[i]
            pointB = nodesList[i+1]

            #print("\n\n")
            for s in range(len(nodesList)):

                if s != len(nodesList) - 1:
                    if i != s:
                        pointC = nodesList[s]
                        pointD = nodesList[s + 1]


                        L1 = line(pointA,pointB)
                        L2 = line(pointC,pointD)

                        R = intersection(L1, L2)

                        valueOfX = 0.0
                        valueOfY = 0.0

                        if R:

                            if pointB[0] > pointA[0]:                                               #line 1 x
                                if R[0] >= pointA[0] and  R[0] <= pointB[0] :
                                    valueOfX = R[0]
                            else:
                                if R[0] >= pointB[0] and R[0] <= pointA[0]:
                                    valueOfX = R[0]


                            if pointB[1] > pointA[1]:
                                if R[1] >= pointA[1] and R[1] <= pointB[1] :
                                    valueOfY = R[1]
                            else:
                                if R[1] >= pointB[1] and R[1] <= pointA[1]:
                                    valueOfY = R[1]


                            if pointD[0] > pointC[0]:  # line 1 x

                                if valueOfX >= pointC[0] and  valueOfX <= pointD[0] :
                                    valueOfX = valueOfX
                            else:
                                if valueOfX >= pointD[0] and valueOfX <= pointC[0]:
                                    valueOfX = valueOfX


                            if pointD[1] > pointC[1]:
                                if valueOfY >= pointC[1] and valueOfY <= pointD[1] :
                                    valueOfY = valueOfY
                            else:
                                if valueOfY >= pointD[1] and valueOfY <= pointC[1]:
                                    valueOfY = valueOfY

                            if valueOfY != 0.0 or valueOfX != 0.0:
                                intersectingPointX.append(valueOfX)
                                intersectingPointY.append(valueOfY)

    plt.scatter(intersectingPointX, intersectingPointY, color='r')
    for xyz in xyzList:                                                     #Print all coordinates with comma seperated...
        print(xyz[0],",",xyz[1],",",xyz[2])

    forIndex = 0
    for data in range(len(intersectingPointY)):

        pointX = round(intersectingPointX[data],2)
        pointY = round(intersectingPointY[data],2)

        print(pointX,",",pointY,",",round(5 + (random() * (5000 -5)),2))
        forIndex = forIndex + 1
        if forIndex == 5:
            forIndex=0


    for node in range(int(numberOfNodes) - 1):  # Print all edges information...
        print(node, ",", (node + 1))

    for node in range(len(intersectingPointX) - 1):  # Print all edges information...
        print(node + int(numberOfNodes), ",", (node + int(numberOfNodes) + 1))


    plt.plot(x, y, color='black')
    plt.title('Road Network for Urban Area\n')
    plt.savefig("graphs.png")
    # plt.show()


def start():                                                                            #Main Function...

    xyzList = []
    #numberOfNodes = input("Enter the No. of Nodes : ")              #Take number of Nodes from users...
    numberOfNodes = 80

                                                                  #Every min and max value given by user...
    for node in range(int(numberOfNodes)):

        x = round(1 + (random() * (5000 -1)),2)                       #generate x coordinate for nodes using random with min and max range
        y = round(1 + (random() * (5000 -1)),2)                       #generate x coordinate for nodes using random with min and max range
        z = round(5 + (random() * (5000 -5)),2)                       #generate x coordinate for nodes using random with min and max range

        xyzList.append([x,y,z])                                     #Add coordinates of every node in list

    drawGraph(xyzList,numberOfNodes)                                             #To draw Urban Area Graph...

if __name__ == '__main__':
    start()
