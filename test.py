#nodes = [x for x in range(int(50))]
#print(nodes)

def altitude(x, y):
    return (((x**2) + (y**2)) / 80) + 5

#alt = altitude(19, 2)
#print(alt)

#for j in range(20):
#    print(j)
#selected = [False] * 500
#print(selected)
#nodes = 50
#nodesArray = [x for x in range(int(nodes))]
#N, V = set(nodesArray), set()
#print(N)

'''
                for j in N:
                    if j in V:
                        continue
                    current_distance = self._distance(neighbor_node, j)
                    if current_distance != distance and current_distance < distance:
                        distance = current_distance
                        indx1 = neighbor_node
                        indx2 = j
'''

import decimal
#decimal.getcontext().prec = 3
#decimal.getcontext().prec = 4
import random
#x = random.uniform(1, 20)
#new_x = round(x, 2)
#print(new_x)

def _create_nodes(nodes):
    nodesSet = set([x for x in range(int(nodes))])
    R = set()
    while nodesSet:
        nodesSet.pop()
        x = round(random.uniform(1, 20), 1)
        y = round(random.uniform(1, 20), 1)
        R.add((x,y))
    return R 

result = _create_nodes(10)
print(result)
