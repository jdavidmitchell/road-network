# importing the requireds modules
import random
import matplotlib.pyplot as plt
from math import sqrt,floor


#no_node = int(input("Enter no of nodes : "))        # getting nodes number from user
#no_edge = int(input("Enter no of edges : "))        # getting edges number from user
no_node = 300
no_edge = 330

# Global Variables
DIAGONAL = round(int((no_node / 75)+1)/10, 1)       # variable for processing
EXIT = int((no_edge/100)+1)*500
GAP = round(int(sqrt(no_node)/2)/10, 1)
coordinatesSet = set({})                            # for total generated nodes
nodesUsedInGraph = set({})                          # the connected nodes only
x_codi = []                                         # stores x-coordinate of each nodes
y_codi = []                                         # stores y-coordinate of each nodes
edgeset = set({})                                   # for edges generated between nodes
nodesPair = set({})                                 #


def nodesGenerator(no_node):
    '''Node Generator function'''
    global coordinatesSet
    n_len = floor(sqrt(no_node))
    while len(coordinatesSet) < no_node:
        x = round(random.randint(0, n_len)/10, 1)
        y = round(random.randint(0, n_len)/10, 1)
        coordinatesSet.add((x,y))

nodesGenerator(no_node)                                 # calling the function

coordinates = list(coordinatesSet)                      # converting set into a list for indexing
print('\n####### Nodes #######')
print(f' X    Y ')
print(f'--------')
for i in range(len(coordinates)):
    x_codi.append(coordinates[i][0])
    y_codi.append(coordinates[i][1])
    print(f'{coordinates[i][0]}, {coordinates[i][1]}')

def edgeGenerator(no_edge):
    ''' Edge Generator function '''
    global edgeset
    global x_codi
    global y_codi
    # global nodesPair
    global nodesUsedInGraph
    global GAP
    global EXIT
    guess = len(x_codi)                             # getting max_guess number
    exit_count = 0
    lastlen = len(edgeset)
    while len(edgeset) < no_edge:                   # while current edges < total edges asked
        temp_x = x_codi.copy()                      # getting a copy of x_codi
        temp_y = y_codi.copy()                      # getting a copy of y_codi
        node_ind1 = random.randint(0, guess - 1)    # getting index of node1 for each edge
        # print(f'node1 = {node_ind1}')
        x1 = x_codi[node_ind1]                      # getting x-coordinate for node1, storing in x1
        y1 = y_codi[node_ind1]                      # getting y-coordinate for node2, storing in y1
        temp_x.pop(node_ind1)                              # removing x-value from x_codi's copy
        temp_y.pop(node_ind1)                              # removing y-value from y_codi's copy

        while(True):                                # break until flag equals true
            # getting a random direction for edge
            # 0 : along y-axis
            # 1 : along x-axis
            # 2 : along DIAGONAL
            choice =random.choice([0, 1, 2])
            if choice == 0 and x1 in temp_x:        # if x1 is exist in x_codi's copy, then
                indx = temp_x.index(x1)             # get the index, store in indx
                x2 = temp_x[indx]                   # get the x2 from x_codi's copy using indx
                y2 = temp_y[indx]                   # get the y2 from y_codi's copy using indy
                # temp = 1                            # used for debugging
                break
            elif choice == 1 and y1 in temp_y:      # if y2 is exist in y_codi's copy, then
                indx = temp_y.index(y1)             # get the index, store in indx
                x2 = temp_x[indx]                   # get the x2 from x_codi's copy using indx
                y2 = temp_y[indx]                   # get the y2 from y_codi's copy using indy
                # temp = 2                          # used for debugging
                break
            elif choice == 2:                       # if choice is for DIAGONAL edges, then
                global DIAGONAL                     # getting global vairable
                option = [(x1+ DIAGONAL, y1-DIAGONAL), (x1+DIAGONAL, y1+DIAGONAL), (x1-DIAGONAL, y1+DIAGONAL), (x1-DIAGONAL, y1-DIAGONAL)]
                opt_ind = random.randint(0, len(option)-1)      # generating a random index for option list

                if opt_ind < len(option)/2:                     # if index is less than half, then
                    x2 = option[opt_ind][0]                     # get the x-coordinate from the option list
                    if x2 in x_codi:                            # check it exists in the x_codi
                        ind = x_codi.index(x2)                  # find it's index, store in ind
                        y2 = y_codi[ind]                        # find the value of y2 in y_codi [ind]
                        if y2 == option[opt_ind][1]:            # checking it's matches with the option list
                            # temp=3                            # used for debugging
                            break
                else:
                    y2 = option[opt_ind][1]                     # get the y-coordinate from the option list
                    if y2 in y_codi:                            # check it exists in the y_codi
                        ind = y_codi.index(y2)                  # find it's index, store in ind
                        x2 = x_codi[ind]                        # find the value of x2 in x_codi [ind]
                        if x2 == option[opt_ind][0]:            # checking it's matches with the option list
                            # temp = 3                          # used fr debugging
                            break

        '''and (abs(x1 - x2) < GAP) and (abs(y2 - y1) < GAP)'''
        if (x2 < x1) and (abs(x1 - x2) < GAP) and (abs(y2 - y1) < GAP):
            edgeset.add(((x2, y2), (x1, y1)))
            # nodesPair.add((node_ind2, node_ind1))
            nodesUsedInGraph.add((x1, y1))
            nodesUsedInGraph.add((x2, y2))
        elif (x1 < x2) and (abs(x1 - x2) < GAP) and (abs(y2 - y1) < GAP):
            edgeset.add(((x1, y1), (x2, y2)))
            # nodesPair.add((node_ind1, node_ind2))
            nodesUsedInGraph.add((x1, y1))
            nodesUsedInGraph.add((x2, y2))
        elif (y1 < y2) and (abs(x1 - x2) < GAP) and (abs(y2 - y1) < GAP):
            edgeset.add(((x1, y1), (x2, y2)))
            # nodesPair.add((node_ind1, node_ind2))
            nodesUsedInGraph.add((x1, y1))
            nodesUsedInGraph.add((x2, y2))
        elif (abs(x1 - x2) < GAP) and (abs(y2 - y1) < GAP):
            edgeset.add(((x2, y2), (x1, y1)))
            # nodesPair.add((node_ind2, node_ind1))
            nodesUsedInGraph.add((x1, y1))
            nodesUsedInGraph.add((x2, y2))
        exit_count = exit_count + 1
        if len(edgeset) > lastlen:
            lastlen = len(edgeset)
            exit_count = 0

        if exit_count >= EXIT:
            print(f'\nERROR : TOO_MUCH_EDGES exception:')
            print(f" Cannot generate {no_edge} edges with {no_node} nodes\n Increase no of nodes, else decrease no of edges")
            exit(-1)

edgeGenerator(no_edge)
#################################
'''New variables for storing the coordinate of nodes'''
X_codi = []
Y_codi = []
Z_codi = []
nodesUsedInGraph = list(nodesUsedInGraph)
print('\n### NODES USED ###')
print(f' X    Y    Z')
print(f'-------------')
for i in range(len(nodesUsedInGraph)):
    X_codi.append(nodesUsedInGraph[i][0])
    Y_codi.append(nodesUsedInGraph[i][1])
    z = round(random.uniform(0, 10), 1)
    Z_codi.append(z)
    #print(f'{nodesUsedInGraph[i][0]}, {nodesUsedInGraph[i][1]} ,{z}')

edgeset = list(edgeset)
for edge in edgeset:
    x1 = edge[0][0]
    y1 = edge[0][1]
    x2 = edge[1][0]
    y2 = edge[1][1]
    index1X = set([i for i, val in enumerate(X_codi) if val == x1])  # getting all index of occurrence of x1 in
    index1Y = set([i for i, val in enumerate(Y_codi) if val == y1])  # getting all index of occurrence of y1 in
    index1 = int(list(index1X.intersection(index1Y))[0])  # getting a intersecting index from both set
    index2X = set([i for i, val in enumerate(X_codi) if val == x2])  # getting all index of occurrence of x2 in
    index2Y = set([i for i, val in enumerate(Y_codi) if val == y2])  # getting all index of occurrence of y2 in
    index2 = int(list(index2X.intersection(index2Y))[0])  # getting a intersecting index from both set
    nodesPair.add((index1, index2))
'''
print('\n#### EDGES ###')
for edge in nodesPair:
    print(edge)

print("\n### Making the plot ... #####")
for i in range(len(edgeset)):                 # plotting each straigh line
    plt.plot([edgeset[i][0][0], edgeset[i][1][0]], [edgeset[i][0][1], edgeset[i][1][1]], color='gray')
plt.scatter(X_codi, Y_codi, color='black', marker='o')  # plotting the nodes
plt.xlabel('x-axis')                                    # labeling x-axis
plt.ylabel('y-axis')                                    # labeling y-axis
#plt.show()                                              # showing
#plt.savefig("roadgen-7.png")
print('\n### DONE ###')
'''
import networkx as nx
g = nx.Graph() #empty graph
for i in range(len(nodesUsedInGraph)):
    g.add_node(i, key=nodesUsedInGraph[i])
for i in nodesPair:
    g.add_edge(i[0], i[1])
#for i in edgeset:
#    g.add_edge((i[0][0], i[1][0]),(i[0][1], i[1][1]))
pos = nx.spring_layout(g)
nx.draw_networkx(g, pos)
plt.savefig("roadgen-11.png")

