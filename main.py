import matplotlib.pyplot as plt
import numpy as np
import random
import argparse
from pprint import pprint

msg = "hello world"
print(msg)
msg2 = msg.capitalize()
print(msg2)
x = np.linspace(0, 20, 100)  # Create a list of evenly-spaced numbers over the range
plt.plot(x, np.sin(x))       # Plot the sine of each x point
#plt.show()
plt.savefig("mygraph.png")
