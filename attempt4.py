import random
import math
from sys import exit

class Attempt():
    def __init__(self, nodes: int, edges=None):
        self.nodes = int(nodes)
        if edges:
            self.edges = int(edges)
        else:
            self.edges = self.nodes + 1
        if self.edges < (nodes - 1):
            print("Not enough edges!")
            exit(0)
        self.nodes_list = []
        self.edges_list = []
        # Will create a loop that will build the initial map/network
        # for the remaining edges, I will do an intersection to add edges
        self.remaining_edges = self.edges - (self.nodes - 1)
        self._create_nodes()
        self._create_graph()

    def _create_altitude(self, x: int, y: int):
        return (((x**2) + (y**2)) / 80) + 5

    def _create_nodes(self):
       node_set = set([x for x in range(self.nodes)])
       while node_set:
           node_set.pop()
           x = round(random.uniform(1, 20), 1)
           y = round(random.uniform(1, 20), 1)
           z = round(self._create_altitude(x, y), 1)
           if (x, y, z) not in self.nodes_list:
               self.nodes_list.append((x,y,z))
    
    def _distance(self, o: int, p: int):
        """
        Get the euclidean distance between two points
        """
        s1 = (self.nodes_list[o][0] - self.nodes_list[p][0]) ** 2
        s2 = (self.nodes_list[o][1] - self.nodes_list[p][1]) ** 2
        return math.sqrt(s1 + s2)

    def _create_graph(self):
        visited = [0]
        node_length = len(self.nodes_list)
        while len(visited) < node_length:
            distance = 999
            ind1 = -1
            ind2 = -1
            for i in visited:
                for j in range(node_length):
                    if j in visited:
                        continue
                    if self._distance(i, j) < distance:
                        distance = self._distance(i, j)
                        ind1 = i
                        ind2 = j
            self.edges_list.append((min(ind1, ind2), max(ind1, ind2)))
            if ind1 not in visited:
                visited.append(ind1)
            else:
                visited.append(ind2)

    def get_nodes(self):
        return self.nodes_list
    
    def get_edges(self):
        return self.edges_list

    #def _on_segment(self, p: int, q: int, r: int):
    #    if r[0] <= max(p[0],)

    def _orientation(self, p: tuple, q: tuple, r: tuple):
        val = ((q[1] - p[1]) * (r[0] - q[0])) - ((q[0] - p[0]) * (r[1] - q[1]))
        if val == 0:
            return 0
        return 1 if val > 0 else -1

    def _intersects(self, X1: float, Y1: float, X2: float, Y2: float, X3: float, Y3: float, X4: float, Y4: float):
        seg1 = ((X1, Y1), (X2, Y2))
        seg2 = ((X3, Y3), (X4, Y4))
        p1, q1 = seg1
        p2, q2 = seg2
        o1 = self._orientation(p1, q1, p2)
        o2 = self._orientation(p1, q1, q2)
        o3 = self._orientation(p2, q2, p1)
        o4 = self._orientation(p2, q2, q1)
        if o1 != o2 and o3 != o4:
            return True
        #if o1 == 0 or o2 == 0 or o3 == 0 or o4 == 0:
        #    return True
        return False

    def add_more_edges(self):
        node_set = set([x for x in range(2500)])
        while node_set:
            node_set.pop()
            if self.remaining_edges < 0:
                break
            B = True
            x = random.randint(0, self.nodes - 1)
            y = random.randint(0, self.nodes - 1)
            for i in self.edges_list:
                if i[0] == x or i[0] == y or i[1] == x or i[1] == y:
                    continue
                if self._intersects(
                    self.nodes_list[x][0],
                    self.nodes_list[x][1],
                    self.nodes_list[y][0],
                    self.nodes_list[y][1],
                    self.nodes_list[i[0]][0],
                    self.nodes_list[i[0]][1],
                    self.nodes_list[i[1]][0],
                    self.nodes_list[i[1]][1]):
                    B = False
                    break
                if B:
                    if (min(x, y), max(x, y)) in self.edges_list:
                        continue
                    self.remaining_edges -= 1
                    if self.remaining_edges < 0:
                        break
                    self.edges_list.append((min(x, y), max(x, y)))


at = Attempt(200, 320)
at.add_more_edges()
#at.draw_map()
nodes = at.get_nodes()
edges = at.get_edges()
#"""
for i in nodes:
    print("{0},{1},{2}".format(i[0], i[1], i[2]))
for e in edges:
    print("{0},{1}".format(e[0], e[1]))
#"""