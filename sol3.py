import random
import math
from sys import exit
import matplotlib.pyplot as plt
import networkx as nx
import time
import cProfile

# get the altitude of a point(x, y)
def altitude(x, y):
    return (((x**2) + (y**2)) / 80) + 5


# get the euclidean distance between 2 points
def distance(i, j):
    return math.sqrt(((nodes[i][0] - nodes[j][0]) ** 2) + ((nodes[i][1] - nodes[j][1]) ** 2))


# create a tree from the nodes
def creategraph():
    visited = [0]
    while len(visited) < len(nodes):
        length = 99999999
        idx1 = -1
        idx2 = -1
        for i in visited:
            for j in range(len(nodes)):
                if j in visited:
                    continue
                if distance(i, j) < length:
                    length = distance(i, j)
                    idx1 = i
                    idx2 = j
        edges.append((min(idx1, idx2), max(idx1, idx2)))
        if idx1 not in visited:
            visited.append(idx1)
        else:
            visited.append(idx2)


# helper function for intersects()
def on_segment(p, q, r):
    if r[0] <= max(p[0], q[0]) and r[0] >= min(p[0], q[0]) and r[1] <= max(p[1], q[1]) and r[1] >= min(p[1], q[1]):
        return True
    return False


# helper function for intersects()
def orientation(p, q, r):
    val = ((q[1] - p[1]) * (r[0] - q[0])) - ((q[0] - p[0]) * (r[1] - q[1]))
    if val == 0:
        return 0
    return 1 if val > 0 else -1


# a function to check whither 2 segments intersect
counter = 0
def intersects(X1, Y1, X2, Y2, X3, Y3, X4, Y4):
    seg1 = ((X1, Y1), (X2, Y2))
    seg2 = ((X3, Y3), (X4, Y4))
    p1, q1 = seg1
    p2, q2 = seg2
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)
    if o1 == 0 or o2 == 0 or o3 == 0 or o4 == 0:
        print("Found a zero\n")
    if o1 != o2 and o3 != o4:
        #print("Found a match")
        #global counter
        #counter = counter + 1
        #print(counter)
        return True
    if o1 == 0 and on_segment(p1, q1, p2):
        return True
    if o2 == 0 and on_segment(p1, q1, q2):
        return True
    if o3 == 0 and on_segment(p2, q2, p1):
        return True
    if o4 == 0 and on_segment(p2, q2, q1):
        return True
    return False


#n = int(input("Enter the number of nodes:"))
#m = int(input("Enter the number of edges:"))
n = 300
m = 600
nodes = []
edges = []
for i in range(n):  # create random nodes but make sure you don't put the same node twice in the list
    while True:
        x = random.uniform(1, 20)
        y = random.uniform(1, 20)
        z = altitude(x, y)
        if (x, y, z) not in nodes:
            nodes.append((x, y, z))
            break
tic = time.perf_counter()
creategraph() # create a tree which will be the base of the graph
toc = time.perf_counter()
print(f"Finished creategraph() in {toc - tic:0.4f} seconds")
g = nx.Graph() #empty graph
for i in range(len(nodes)):
    g.add_node(i, key=nodes[i])
for i in edges:
    g.add_edge(i[0], i[1])
pos = nx.spring_layout(g)
nx.draw_networkx(g, pos)
plt.savefig("sol-5-1st.png")
# Count intersection time
tic2 = time.perf_counter()
m -= (n - 1) # number of edges needed to make a tree
if m < 0:
    print("Not enough edges.")
    exit(0)
for x in range(len(nodes)): # pick 2 points randomly and create an edges between them, if this new created edge won't intersect with the already constructed edges
    if m < 0:
        break
    for y in range(len(nodes)):
        if x == y:
            continue
        if m < 0:
            break
        B = True
        for i in edges:
            if i[0] == x or i[0] == y or i[1] == x or i[1] == y:
                continue
            if intersects(nodes[x][0], nodes[x][1], nodes[y][0], nodes[y][1], nodes[i[0]][0], nodes[i[0]][1], nodes[i[1]][0], nodes[i[1]][1]):
                B = False
                break
        if B:
            if m < 0:
                break
            if (min(x, y), max(x, y)) in edges:
                continue
            m -= 1
            if m < 0:
                break
            edges.append((min(x, y), max(x, y)))
            break
toc2 = time.perf_counter()
print(f"Finished intersection loops in {toc2 - tic2:0.4f} seconds")
tic3 = time.perf_counter()
for i in range(len(nodes)): # if we still need to put edges in the graph then for every nodes pick its closest node and put an edges between them
    m -= 1
    if m < 0:
        break
    dis = 99999999
    idx = -1
    for j in range(len(nodes)):
        if i == j or (min(j, i), max(i, j)) in edges:
            continue
        if distance(i, j) <= dis:
            dis = distance(i, j)
            idx = j
    #min1 = min(idx, i)
    #max1 = max(i, idx)
    #print("{0}, {1}".format(min1, max1))
    edges.append((min(idx, i), max(i, idx)))
toc3 = time.perf_counter()
print(f"Finished remainder loops in {toc3 - tic3:0.4f} seconds")
if m > 0:
    print("Too many edges.")
    exit(0)

#printing for the console
#for i in nodes:
#    print("{0}, {1}, {2}".format(i[0], i[1], i[2]))
#for i in edges:
#    print("{0}, {1}".format(i[0], i[1]))

#plotting using matplotlib starts here
"""
x = [i[0] for i in nodes]
y = [i[1] for i in nodes]
plt.plot(x, y, 'ro')
plt.axis('equal')
x_line = []
for i in edges:
    x_line.append(nodes[i[0]][0])
    x_line.append(nodes[i[1]][0])
y_line = []
for i in edges:
    y_line.append(nodes[i[0]][1])
    y_line.append(nodes[i[1]][1])
for i in range(0, len(x_line), 2):
    plt.plot(x_line[i:i+2], y_line[i:i+2], 'ro-')
#plt.show()
#plotting using matplotlib ends here
"""

g = nx.Graph() #empty graph
for i in range(len(nodes)):
    g.add_node(i, key=nodes[i])
for i in edges:
    g.add_edge(i[0], i[1])
pos = nx.spring_layout(g)
nx.draw_networkx(g, pos)
plt.savefig("sol-5-2nd.png")
